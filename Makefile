#
#	Makefile проекта 
#

PROJECT = ppd

#
#	Setting
#

CFLAGS = -Wall -pedantic -g -funsigned-bitfields -Wno-write-strings -Wno-long-long -pg -O3

CC = g++ $(CFLAGS)

SRC = ./src
DEST = ./appendix

# 
# 	Setting
# 
INCPATH = -I. -I./include -I/usr/local/include

# 
# 	Setting
# 
LIBPATH = -L. -L/usr/local/lib
LIBS = -ldts ./libjansson.a -lpthread -lrt

# 
# 	Setting
# 

# 
# 	Переменная - все объектные файлы комипилируемого проекта
# 
OBJECTS = \
	$(DEST)/json.o $(DEST)/dts.o $(DEST)/handlers.o $(DEST)/setopt.o $(DEST)/getopt.o $(DEST)/opt.o $(DEST)/fgu.o 
 
# 
# 	Setting
# 
ppd : obj main.o 
	$(CC) $(INCPATH) -o $(DEST)/$(PROJECT) $(OBJECTS) $(DEST)/main.o $(LIBPATH) $(LIBS)

# 
# 	Объектные файлы исполнимого файла (не из состава бибилиотеки)
# 
obj	: \
	json.o dts.o handlers.o getopt.o setopt.o opt.o fgu.o 




# 
# 	Объектные файлы компилируемой библиотеки
# 
clean	: 
	rm -f $(DEST)/*.o $(DEST)/*.so $(DEST)/$(PROJECT) $(DEST)/*.a $(DEST)/json


 
# 
# 	
# 
 
main.o	: main.c 
	$(CC) -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/main.o main.c

json.o 	: ./src/json.c
	$(CC)  -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/json.o ./src/json.c

dts.o : ./src/dts.c
	$(CC) $(INCPATH) -c -o $(DEST)/dts.o ./src/dts.c

handlers.o : ./src/handlers.c
	$(CC) $(INCPATH) -c -o $(DEST)/handlers.o ./src/handlers.c
fgu.o : ./src/fgu.c
	$(CC) $(INCPATH) -c -o $(DEST)/fgu.o ./src/fgu.c
hsearch.o : ./src/hsearch.c
	$(CC) $(INCPATH) -c -o $(DEST)/hsearch.o ./src/hsearch.c

# 
# 	Модуль GETOPT
# 
setopt.o	: ./getopt/setopt.c ./getopt/setopt.h
	$(CC) -D_eXAMPLE_ -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/setopt.o ./getopt/setopt.c 

getopt.o	: ./getopt/getopt.c ./getopt/getopt.h
	$(CC) -D_dEBUG_ -D_eXAMPLE_ -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/getopt.o ./getopt/getopt.c 

opt.o	: ./getopt/opt.c ./getopt/opt.h
	$(CC) -D_eXAMPLE_ -D_USER_OPTION_ $(INCPATH) -c -o $(DEST)/opt.o ./getopt/opt.c 

 

