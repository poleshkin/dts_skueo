#ifndef _DTS_H
#define _DTS_H


#include "dtsclient.h"
#include "jansson.h"
/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

struct _htol_ {
	int 		state;
	unsigned 	index;
	char 		sig[4];
};

/*
 *	Data declaration
 */	


/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif
void 		make_dtsclient(struct _htol_ *);
int 		rcvFunc(void *argPtr, Value &value, int32_t chnlId);
int 		make_an(struct _mcase_ *);
int 		make_bin(struct _mcase_ *);
int			make_i(struct _mcase_ *);
int 		send_signal(struct _mcase_ *);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
