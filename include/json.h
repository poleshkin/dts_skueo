#ifndef _JSON_H
#define _JSON_H

#include "jansson.h"
#include "dtsclient.h"
#include "time.h"
#include "locale.h"

/*
 *	Macros difinition
 */
#define SUCCESS        0
#define FAILURE        -1

/*
 *	Type declaration
 */

struct _mcase_ {
    int     sec;
    int     ms;
    int     fault;
    int     quality;
    char    *val;
    char    *kks;
    int     type;
    int     (*func)(struct _mcase_ *);
    int     signum;
    Value   *value;
};
 
struct _hconf_ {
    char kks[32];
    char file[64];
    void (*func)(FILE*, char *, char *);
};

/*
 *	Data declaration
 */

extern struct _hconf_ htuple[10000];

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif
void 	load_hcfg(char *);
void     open_low_events(char *);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
