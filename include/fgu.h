#ifndef _FGU_H
#define _FGU_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */	

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void open_fgu_events(char *);
void write_apw(FILE*, char *, char *);
void write_ou(FILE*, char *, char *);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
