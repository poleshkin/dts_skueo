#ifndef _HANDLERS_H
#define _HANDLERS_H

#include "dts.h"
#include "json.h"
#include "fgu.h"

/*
 *	Macros difinition
 */

#define SUCCESS	0
#define FAILURE	-1

#define FREE	0
#define BUSY	1

/*
 *	Type declaration
 */

struct _node_ {
    struct _node_   *next;
    const char      *tok;
    unsigned short  delta;
    void*           dest;
    const char*     (*func)(void* dest,const char *msg);
};

/*
 *	Data declaration
 */	
extern int warnings, success;
/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif
void	make_read_thread(char*);
void 	handler_signals(int);
int 	parse_data(struct _fnode_ *, fd_set *);
void*	swap(char *);
void* 	read_events(void* arg);



#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */
