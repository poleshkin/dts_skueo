#include "handlers.h"
#include "getopt/opt.h"
#include "locale.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <dirent.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>

//struct _eventf_ list = {NULL, NULL};
int warnings = 0;
int success = 0;

struct _hconf_ 	htuple[10000];
struct _htol_ 	hbuf[10] = { 
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
	{ 0, 0},
};

int main(int argc, char *argv[])
{	
	int 				i, rc=0;
	struct sigaction 	sa;
	FILE 				*fgu;
	char 				file_in[128];
	struct _hconf_ 		*p = NULL;
	
	memset(&sa,0,sizeof(sa));
	sa.sa_handler=handler_signals;
	sigaction(SIGINT,&sa,NULL);
	sigaction(SIGHUP,&sa,NULL);
	sigaction(SIGQUIT,&sa,NULL);
	sigaction(SIGTERM,&sa,NULL);
	sigaction(SIGUSR1,&sa,NULL);

	setlocale(LC_NUMERIC,"C");
	if (getoption(option, argc, argv) != OPT_SUCCESS)
	{
		puthelp();
	}
	
	openlog("ppd", LOG_PID | LOG_CONS, LOG_LOCAL2);

	rc = sprintf( file_in, "%s%s", _GETOPTS(option, OPT_FGU), "in");
    if ( rc < 1 ){
    	syslog(LOG_ERR, "load_cfg: sprintf: Failed to form fgu in path with %s%s", _GETOPTS(option, OPT_FGU), "in" );
        exit(-1);
    }

	load_hcfg(_GETOPTS(option, OPT_CONF));							//get config for high->low signals
	make_dtsclient(hbuf);
	make_read_thread(_GETOPTS(option, OPT_LOW));
	open_fgu_events(_GETOPTS(option, OPT_FGU));
	//open_low_events(_GETOPTS(option, OPT_EVENTS));				//open files with events (low->high signals)
		
	while ( 1 ) {
		for ( i = 0; i < 10; i++) {
			if ( hbuf[i].state != FREE ) {
				p = &htuple[hbuf[i].index];
				fgu = fopen(file_in, "w");
				if (fgu) {
					p->func(fgu, p->kks, hbuf[i].sig);
					hbuf[i].state = FREE;
					fclose(fgu);
				} else {
					syslog(LOG_ERR, "failed fopen FGU in file %s with: %s", file_in, strerror(errno));
					exit(-1);
				}
			}
		}
		printf("Warnings = %d\t Success = %d\n", warnings, success);
		sleep(1);
	}

	return rc;
}
