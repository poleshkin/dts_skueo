#include "handlers.h"
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <pthread.h>

/*
 *	Data definition:
 */

#define BUFLEN 4096*16

struct _mcase_ 	mcase;

__inline__ static const char* setint(unsigned *dest,char* msg)
{
		char tok[32];
		char *p=tok;
		
		while (*msg != ',' && *msg != '}')
		{
				*p=*msg;
				p++;
				msg++;
		}
		*p='\0';
		*dest=atoi(tok);
		
		return msg+1;
}


__inline__ static const char* setstr(char **dest,char *msg)
{
		*dest=msg;

		while (*msg != '"' && *msg != ',')
		{
				msg++;
		}
		*msg='\0';
		
		return msg+1;
}
														
static struct _node_ tokens[7]={
		{&tokens[1],"sec", 5, &mcase.sec, (const char* (*)(void*, const char *))setint},
		{&tokens[2],"ms", 4,&mcase.ms,(const char* (*)(void*, const char *)) setint},
		{&tokens[3],"fault",7, &mcase.fault,(const char* (*)(void*,const  char *))setint},
		{&tokens[4],"value",7, &mcase.val,(const char* (*)(void*, const char *))setstr},
		{&tokens[5], "kks",6, &mcase.kks,(const char* (*)(void*,const  char *))setstr},
		{&tokens[6], "type",6, &mcase.type,(const char* (*)(void*,const  char *))setint},
		{NULL, "snum",6, &mcase.signum,(const char* (*)(void*,const  char *))setint}
};

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name        - get_quality

Usage       - make_bin make_an

Prototype in    - json.c   

Description - Вычисляет качество сигнала по ошибке в сообщении
*---------------------------------------------------------------------*/

static int get_quality(int fault)
{
	static short 	fqtable[4]={0x20,0x1,0x2,0x100};

	return fqtable[fault];
}

/*---------------------------------------------------------------------*

Name        - parse_msg

Usage       - parse_data

Prototype in    - json.c   

Description - Разбирает вычитанный буфер эвентов. Разбивает по патронам, заполняя список с токенами, и формирует кейсы сообщений    

*---------------------------------------------------------------------*/
static int parse_msg(char *msg, int len)
{
		int rc=0;
		struct _node_ *p;
		char *buf=(char *)msg;
		
		do
		{
			 p=tokens;
			 while (p)
			 {
					 buf=strstr(buf,p->tok);
					 if (buf == NULL)
					 {
							p=NULL;
							rc = FAILURE;
					 }
					 else
					 {
							buf=(char *)p->func(p->dest, buf+p->delta);
							p=p->next;
					 }
			 }

			 switch (mcase.type)
			 {
						case 1:
								mcase.func=&make_an;
								break;

						case 3:
								mcase.func=&make_i;
								break;
			 }
			 mcase.quality = get_quality(mcase.fault);
			 
			 if (rc != FAILURE && mcase.signum != -1) 
			 {    
						rc=send_signal(&mcase);

			 } 
			 else 
			 {
					 syslog(LOG_ERR,"failed to form case with kks %s signum %d val %s fault %d", mcase.kks, mcase.signum, mcase.val, mcase.fault);
			 }
		} while ( rc != FAILURE && (len - (buf-msg)) > 50);
		
		
		return rc;
}

/*---------------------------------------------------------------------*

Name        - parse_data

Usage       - main

Prototype in    - handlers.c   

Description - Находит дескриптор файла, в котором произошли изменения и вызывает обработчик данных для него
*---------------------------------------------------------------------*/
/*int parse_data(struct _fnode_ *head, DTSClient& client, fd_set *set)
{   
	int             len,rc=0;
	struct _fnode_  *p = head;

		while (p)
		{   
				if (FD_ISSET(p->fd, set) )
				{
					if ((len=read(p->fd,buf,BUFLEN)) > 0)
					{	
						if (parse_msg(buf,client,len) == -1)
						{
									syslog(LOG_ERR, "parse_msg returned -1");
						}
					} 
					else
					{
							syslog(LOG_ERR, "read return -1: %s", strerror(errno));
					}
				}
				p=p->next;
		}

		return rc;
}*/

void* read_events(void* arg)
{
	char 		*path = (char *)arg;
	char 		buf[BUFLEN];
	int 		fd = -1, len;
	fd_set		fds;

	while ( 1 ) {
		if ( fd < 0 ) {
			fd = open(path, O_RDONLY);
			sleep(1);
		} else {
			FD_ZERO(&fds);
			FD_SET(fd, &fds);;
			if ( select(fd+1, &fds, NULL, NULL, NULL) > 0 ) {
				len = read(fd, buf, BUFLEN);
				if ( len > 0 ) {
					if ( parse_msg(buf, len) != SUCCESS ) {
						syslog(LOG_ERR, "parse_msg returned -1");
					}
				} else if ( len == 0 ) {
					sleep(1);
				} else {
					close(fd);
					fd = -1;
				}
			} 
		}
	}

	return NULL;
}

void make_read_thread(char *path)
{
	pthread_t 	thread_id;
	int 		rc;

	rc = pthread_create(&thread_id, NULL, read_events, path);
	if ( rc != SUCCESS ) {
		syslog(LOG_ERR, "pthread_create failed: %s with path %s", strerror(rc), path);
		exit(-1);
	}
}
/*---------------------------------------------------------------------*

Name        - swap

Usage       - dts.c fgu.c

Prototype in    - handlers.c   

Description - Переставляет байты местами
*---------------------------------------------------------------------*/
void* swap(char *data)
{
		char tmp=data[3];
		
		data[3]=data[0];
		data[0]=tmp;
		tmp=data[2];
		data[2]=data[1];
		data[1]=tmp;
												
		return data;
}
														
void handler_signals(int sig_num)
{
		printf("Program was stopped by signal\n");
		exit(0);
}
