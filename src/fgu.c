#include "handlers.h"

#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>


char path[32];

void write_apw(FILE *file, char *kks, char *data)
{	
	int rc;

    rc = fprintf(file,"0:%s:%f", kks, *((float *)swap(data)));
    if ( rc < 1 ) {
    	syslog(LOG_ERR, "write_apw failed: kks %s data = %f", kks, *((float *)swap(data)));
        exit(-1);
    }
}

void write_ou(FILE *file, char *kks, char *data)
{	
	int rc;

    rc = fprintf(file, "0:%s:%d", kks, *((unsigned *)swap(data)));
    if ( rc < 1 ) {
    	syslog(LOG_ERR, "write_ou failed: kks %s data = %d", kks, *((unsigned *)swap(data)));
        exit(-1);
    }
}

void open_fgu_events(char *dir)
{
    int rc;

    rc = sprintf( path, "%s%s", dir, "x0.event");
    if ( rc < 1 ){
        syslog(LOG_ERR, "open_fgu_event: sprintf failed to make fgu event path %s", path);
        exit(-1);
    } 

    make_read_thread(path);
    /*rc = pthread_create(&thread_id, NULL, read_events, path);
    if ( rc != SUCCESS ) {
        syslog(LOG_ERR, "pthread_create failed: %s with path %s", strerror(rc), path);
        exit(-1);
    }*/
}
/*void write_fgu(struct _hconf_ *sig, char *data)
{
	while (!event) {
		event = fopen(sig->file, "r+");
		if (!event) {
  			syslog(LOG_ERR, "failed fopen FGU file %s with: %s", sig->file, strerror(errno));
  			sleep(1);
		}
	}
	setlinebuf(event);

	sig->func(sig->kks, data);

    fclose(event);
    event = NULL;
}*/
