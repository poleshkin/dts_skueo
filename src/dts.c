#include <time.h>
#include <math.h>
#include "handlers.h"
#include <syslog.h>

/*
 *	Data definition:
 */

Value   value;
static DTSClient *client;
/*
 *	Function(s) definition:
 */
/*---------------------------------------------------------------------*

Name        - make_dtsclient

Usage       - main

Prototype in    - main.c  

Description - Вызов конструктора класса для дтс клиента

*---------------------------------------------------------------------*/
void make_dtsclient(struct _htol_ *buffer)
{   
    //DTSClient client(rcvFunc, NULL, NULL, NULL, 49001);
    client = new DTSClient(rcvFunc, buffer, NULL, NULL, 49001);
}

/*---------------------------------------------------------------------*

Name        - load_an_sig

Usage       - parse_msg

Prototype in    - json.c   

Description - Формирует аналоговый сигнал для отправки в ДТС

*---------------------------------------------------------------------*/

int make_an(struct _mcase_ *an)
{
    float       val;
    int         rc=0;
    
    //printf("mcase->val=%s\n", an->val);
    val = atof(an->val);
    if (finitef(val)) {   
        rc = makeAValue( &value, an->signum, an->quality, val, an->sec, an->ms);
    } else {
        syslog(LOG_ERR, "make_an : value is not a finite number: %s", an->val);
        rc=-1;
    }

    return rc;
}
/*---------------------------------------------------------------------*

Name        - load_bin_sig

Usage       - parse_msg

Prototype in    - json.c   

Description - Формирует дискретный сигнал для отправка в DTS  

*---------------------------------------------------------------------*/
int make_bin(struct _mcase_ *bin)
{
    int             val;
    int             rc=0;
    int             len = strlen(bin->val);

    val = atoi(&bin->val[len-1]);
    if (val != 0 && val !=1) {
        syslog(LOG_ERR,"make_bin : value is not a bin signal: %s", bin->val);
        rc=-1;
    } else {   
        rc = makeBValue( &value, bin->signum, bin->quality, val, bin->sec, bin->ms);
    }   

    return rc;
}

int make_i(struct _mcase_ *in)
{
    int	val;
    int rc=0;

    val = atoi(in->val);
    if (val <= 65535) {
        rc = makeIValue( &value, in->signum, in->quality, val, in->sec, in->ms);
    } else {
        syslog(LOG_ERR, "make_i : value is %s", in->val);
        rc=-1;
    }

    return rc;
}
                                                                
/*---------------------------------------------------------------------*

Name        - send_signal   

Usage       - json.c

Prototype in    - dts.c

Description - Отправляет сформированный сигнал в dts   

*---------------------------------------------------------------------*/
int send_signal(struct _mcase_ *mcase)
{
    int             rc=0;

    if (mcase->func(mcase) == 0) {   
        switch( client->put( &value ) ) { 
            case -1 :
                printf("ERROR\n");
                syslog(LOG_ERR, "Analog value put error: %d", client->getError());
                break; 

            case 1 :
                //printf("SIGNUM WARNING%d\n", mcase->signum);
                warnings++;
                //printf("WARNING=%d\n", client->getError());
                if (rc != 3) {
                    syslog(LOG_ERR, "Analog value put warning: %d", client->getError());
                }
                rc = 0;
                break;

            case 0:
                //if (mcase->signum == 20361 ) {
                    //printf("SUCCESS signum %d\t val %s\n", mcase->signum, mcase->val);
                //}
                success++;
        }
    } else {   
        syslog(LOG_ERR, "send_signal: Signal was not created: %s", mcase->val);
        rc= -1;
    }

    return rc;
}
/*---------------------------------------------------------------------*

Name		- rcvFunc	

Usage		- main.c

Prototype in	- dts.c

Description	- Отображает, поступающие в ДТС значения    

*---------------------------------------------------------------------*/

int rcvFunc(void *argPtr, Value &value, int32_t chnlId)
{
    static struct _htol_ *buffer = (struct _htol_ *)argPtr;
    int i = 0;
	
	switch(value.type) {
		case Ana_VT :
        {   
	       AData *dataPtr = NULL;
	       value.getData(dataPtr);
        }
			break;

		case Bin_VT :
        {
			BData *dataPtr = NULL;
			value.getData(dataPtr);
        }
			break;

		case Int_VT :
        {
			IData *dataPtr = NULL;
			value.getData(dataPtr);
        }
			 break;

		case Grp_VT :
        {
			GData *dataPtr = NULL;
			value.getData(dataPtr);
			if (dataPtr != NULL) {
			    if (value.idx == 8 && dataPtr->groupType==11 && dataPtr->size==8) {
                    
                    for (i = 0; i < 10 ; i++ ) {
                        if ( buffer[i].state == FREE ) {
                            buffer[i].index = *((unsigned *)swap((char *)dataPtr->value));
                            memcpy(buffer[i].sig, &dataPtr->value[4], 4);
                            buffer[i].state = BUSY;
                            break;
                        }
                    }
                }
			}
    	}
			break;

		default : break;
	}
    
	return 0;
}

