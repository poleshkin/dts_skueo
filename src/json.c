#include "handlers.h"
#include "getopt/opt.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>


/*
 *	Data definition:
 */     

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name        - load_hcfg

Usage       - main.c

Prototype in    - json.c   

Description - Загружает конфигурацию для сигналов сверху вниз 

*---------------------------------------------------------------------*/
void load_hcfg(char *path)
{
    json_t          *root, *array, *note, *tmp;
    json_error_t    error;
    const char      *kks;
    int             signum, func; 
    unsigned        i;        
    
    
    root=json_load_file(path, JSON_ENCODE_ANY, &error);
    if(root && json_typeof(root)==JSON_OBJECT) 
    {
	    array=json_object_get(root,"kks");
	    if (array && json_typeof(array)==JSON_ARRAY) 
        {
		    for (i=0; i < json_array_size(array); i++) 
            {
			    note=json_array_get(array,i);
			    if (note && json_typeof(note) == JSON_OBJECT) 
                {
                    tmp = json_object_get(note,"signum");
                    if ( tmp && json_typeof(tmp) == JSON_INTEGER) {
                        signum = json_integer_value(tmp);
                    } else {
                        syslog(LOG_ERR, "Bad json syntax: The element %d has bad signum", i);
                        exit(-1);
                    }

                    tmp = json_object_get(note,"func");
                    if ( tmp && json_typeof(tmp) == JSON_INTEGER) {
                        func = json_integer_value(tmp);
                    } else {
                        syslog(LOG_ERR, "Bad json syntax: The element %d has bad func", i);
                        exit(-1);
                    }

                    tmp = json_object_get(note,"kks");
                    if ( tmp && json_typeof(tmp) == JSON_STRING) {
                        kks = json_string_value(tmp);
                    } else {
                        syslog(LOG_ERR, "Bad json syntax: The element %d has bad kks", i);
                        exit(-1);
                    }

                   /* tmp = json_object_get(note,"file");
                    if ( tmp && json_typeof(tmp) == JSON_STRING) {
                        file = json_string_value(tmp);
                    } else {
                        syslog(LOG_ERR, "Bad json syntax: The element %d has bad fgu path", i);
                        exit(-1);
                    }*/

			        memcpy(htuple[signum].kks, kks, strlen(kks));
                   /* rc = sprintf( htuple[signum].file, "%s%s", _GETOPTS(option, OPT_FGU), file);
                    if ( rc < 1 ){
                        syslog(LOG_ERR, "load_cfg: sprintf: Failed to form fgu path with %s%s", _GETOPTS(option, OPT_FGU), file );
                        exit(-1);
                    }*/

			        switch (func) 
                    {
				        case 6:
				            htuple[signum].func = &write_apw;
				            break;

				        case 7:
				            htuple[signum].func = &write_ou;
				            break;

                        default:
                            syslog(LOG_ERR, "Bad json syntax: The element %d has bad func = %d", i, func);
                            exit(-1);
			        }
			    } 
                else 
                {
                    syslog(LOG_ERR, "Bad json syntax: failed to get element from kks array in configuration for index %d", i);
			        exit(-1);
			    }
            }

            json_decref(root);
		    json_decref((json_t *)&error);

	    } 
        else 
        {
            syslog(LOG_ERR, "Bad json syntax: failed to get the array \"kks\" from configuration");
            exit(-1);
	    }
	} 
    else 
    {
        syslog(LOG_ERR, "json_load_file error: %s %s", error.text, path);
	    exit(-1);
    }
}

/*---------------------------------------------------------------------*

Name        - open_event_file

Usage       - main.c

Prototype in    - json.c   

Description - Разбирает конфигурацию эвент файлов из json-файла    

*---------------------------------------------------------------------*/
/*void open_low_events(char *path)
{
    size_t i;
    json_t *root, *array = NULL, *element = NULL;
    json_error_t error;

    root=json_load_file(path, JSON_ENCODE_ANY, &error);
    if ( root && json_typeof(root) == JSON_OBJECT ) {
        array=json_object_get(root,"event_files");
        if ( array && json_typeof(array) == JSON_ARRAY) {
            for ( i = 0; i < json_array_size(array); i++) {
                element = json_array_get(array, i);
                if (element && json_typeof(element) == JSON_OBJECT ) {
                    element=json_object_get(element,"path");
                    if (element != NULL && json_typeof(element) == JSON_STRING) {
                        make_pthread(json_string_value(element));
                    } else {
                        syslog(LOG_ERR, "Bad json syntax: failed to get the path of the event file for the index %d", i );
                        exit(-1);
                    }
                } else {
                    syslog( LOG_ERR, "Bad json syntax: failed to get the element of the eventfiles array for the index %d", i );
                    exit(-1);                       
                }
            }
        } else {
            syslog(LOG_ERR, "Bad json syntax: the array of eventfiles was not found");
        }
    } else {
        syslog(LOG_ERR, "Failed to load configuration file with path: %s and error: %s", path, error.text);
    }

    json_decref(element);
    json_decref(array);
    json_decref(root);
    json_decref((json_t *)&error);
}*/

