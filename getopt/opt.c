
#include <stdio.h>

#include "opt.h"
#include "getopt/setopt.h"


/*
 *	Data definition:
 */

static char 	*help[] = {
	"Usage: \"PPD\" -e [PATH] -c [PATH] -f [PATH]\n\n",
	"-h\t--help		display this help and exit\n",
#if defined ( _USER_OPTION_ )
	"-e\t--event 		path to low events file\n",
	"-c\t--cfg			open signals configuration\n",
	"-f\t--fgu 			FGU directory\n",
#endif
	NULL };

#if defined ( _USER_OPTION_ )
char		lowevents[128] = "/dev/null";
char		sconf[128] = "/dev/null";
char		fgudir[128] = "/dev/null";


#endif

struct _option_	option[] = {
	{   1, "-h", "--help",		help,		GETOPT_FLAG, 	(setopt_t) getmsg 	},
#if defined ( _USER_OPTION_ )
	{	2, "-e", "--event", 	lowevents,	GETOPT_MSG, 	(setopt_t) setstr 	},
	{	3, "-c", "--cfg", 		sconf, 		GETOPT_MSG, 	(setopt_t) setstr 	},			
	{	4, "-f", "--fgu", 		fgudir, 	GETOPT_MSG, 	(setopt_t) setstr 	},
#endif
	{ 0, NULL, NULL, NULL, 0, NULL }
};

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void puthelp(void)
{
	puthlp(help);
}

