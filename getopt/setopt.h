#ifndef _SET_OPTION_H
#define _SET_OPTION_H

#include "getopt/option.h"

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

typedef int (*setopt_t)(struct _option_ *, void *);

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */
extern void puthlp(char **);
extern int setstr(struct _option_ *, char *);

#ifdef __cplusplus
extern "C" {
#endif

int getmsg(struct _option_ *, char *);

#ifdef __cplusplus

}
#endif

#endif /* _SET_OPTION_H */
