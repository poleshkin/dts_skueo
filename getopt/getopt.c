
#include <stdio.h>
#include <string.h>
#include "getopt/getopt.h"

/*
 *	Data definition:
 */

#if defined ( _DEBUG_ ) 
static char	*typemsg[] = {
	"_",
	"FLAG",
	"MESSAGE",
	"NUMBER"
};
#endif


/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static int isoption(struct _option_ *opt, char *s)
{
	int	rc = OPT_FAILURE;
	
	while ( opt->id != 0 ) {
		if ( (strcmp(opt->sname, s) == 0) || (strcmp(opt->lname, s) == 0) ) {
			rc = OPT_SUCCESS;
			break;
		}
		opt++;
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static void *checkopt(struct _option_ *opt, char *s)
{
	while ( opt->id != 0 ) {
		if ( (strcmp(opt->sname, s) == 0) || (strcmp(opt->lname, s) == 0) ) {
			return opt;
		}
		opt++;
	}
	
	return NULL;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static int getopt(struct _option_ *opt, char *s)
{
	int	rc = 0;
	
	if ( opt->handler != NULL ) {
		opt->handler(opt, s);
	} else {
		rc = OPT_FAILURE;
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
#if defined (_DEBUG_)
void oprint(struct _option_ *opt)
{
	while ( opt->id != 0 ) {
		fprintf(stderr, "GETOPT: option id = %d, long name = \"%16s\", short name = \"%8s\"\n", 
				opt->id, opt->lname, opt->sname);
		opt++;
	}
}
#endif

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static int checkval(struct _option_ *opt, int type, char *vals)
{
	int	rc;
	
	if ( ( type != GETOPT_FLAG ) && (( vals == NULL ) || ( isoption(opt, vals) == OPT_SUCCESS )) ) {
		rc = OPT_FAILURE;
	} else {
		rc = OPT_SUCCESS;
	}
#if defined (_DEBUG_)
	fprintf(stderr, "GETOPT: type=\'%10s\' value=\'%s\'\n", typemsg[type], vals);
#endif

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static int _getoption(struct _option_ *opt, int n, char **s)
{
	int		i, rc = OPT_FAILURE;
	struct _option_	*option;

	for ( i = 0; i < n; i++ ) {
		
		option = (struct _option_ *)checkopt(opt, s[i]);
		
		if ( option != NULL ) {
#if defined (_DEBUG_)
			fprintf(stderr, "GETOPT: option id = %d, \"%s\"\n", option->id, s[i+1]);
#endif
			if ( checkval(opt, option->type, s[i+1]) != OPT_SUCCESS ) {
				fprintf(stderr, "GETOPT: error occurred on option  \'%s\':\'%s\'\n", option->sname, option->lname);
				rc = OPT_FAILURE;
				break;
			} else {
				rc = getopt(option, s[i+1]);
			}
		}
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int getoption(struct _option_ *opt, int n, char **s)
{
	int	rc; 

	if ( n > 1 ) { 
		rc = _getoption(opt, n, s);
	} else {
		rc = OPT_FAILURE;
	}

	return rc;
}

