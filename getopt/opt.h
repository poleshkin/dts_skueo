#ifndef _OPT_H
#define _OPT_H

#include "getopt/getopt.h"


/*
 *	Macros difinition
 */


/*
 *	Type declaration
 */
enum {
	OPT_HELP = 0
		
#if defined (_USER_OPTION_)
	,
	OPT_LOW = 1,
	OPT_CONF = 2,
	OPT_FGU = 3
#endif
		
#if defined (_EXAMPLE_)
	,
	OPT_FLAG,
	OPT_NUMBER,
	OPT_MSG
#endif
};
/*
 *	Data declaration
 */

extern struct _option_ option[];

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void puthelp(void);

#ifdef __cplusplus
}
#endif

#endif /* _OPT_H */
