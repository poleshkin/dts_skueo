#!/bin/bash

files=$(find /etc/el/ -not -iwholename '*events' -type f)

while [ 1 ]; do 
	for i in $files; do
		echo "{\"ts\":{ \"sec\":\"$(date +"%s")\",\"ms\":\"$(expr $(date +%N) / 1000000)\"},\"fault\":\"2\",\"value\":\"$(cat $i)\",\"type\":\"event\",\"db\":{ \"nick\":\"Т ХЛД ГЦТ2\",\"name\":\"Температура холодной нитки в петле (2 канал)\",\"block\":\"ПАЭ-02\"},\"kks\":\"$(echo $i | cut -d "/" -f5,6 | tr -d '/')\"}" >> /tmp/fifo
	done
	sleep 10
done


exit 0